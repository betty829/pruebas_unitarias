import unittest
from nsBetty.Octal import Octal

class Test_Octal(unittest.TestCase):
    def setUp(self):
        self.octal = Octal()

    def test_hex_to_octal(self):
        self.assertEqual(self.octal.hex_to_octal("F"),"17")

    def test_bin_to_octal(self):
        self.assertEqual(self.octal.bin_to_octal([1,0,0]),"4")

    def test_split_array(self):
        self.assertEqual(self.octal.split_array([0,0,1,1,1,0,0,1,0]),[[0, 0, 1], [1, 1, 0], [0, 1, 0]])

    def test_letra_a_numeros(self):
        self.assertEqual(self.octal.letra_a_numeros("B"),11)
    
    def test_to_binario(self):
        self.assertEqual(self.octal.to_binario(10),[1,0,1,0])

    def test_invertir(self):
        self.assertEqual(self.octal.invertir([1,0,0]),[0,0,1])

