class Octal:

    def __init__(self):
        return
                

    def hex_to_octal(self, hexa):
        binario = []
        for letra in hexa:
            binario.extend(self.to_binario(self.letra_a_numeros(letra)))
        self.completar_a_3(binario)
        return self.bin_to_octal(binario)


    def completar_a_3(self, arreglo):
        residuo = len(arreglo) % 3
        if residuo != 0:
            faltantes = 3 - residuo
            for i in range(0, faltantes):
                arreglo.insert(0, 0)


    def bin_to_octal(self, binario):
        octal = ""
        if len(binario) == 3:
            return str(binario[0] * 4 + binario[1] * 2 + binario[2] * 1)
        else:
            for parte in self.split_array(binario):
                octal = octal + self.bin_to_octal(parte)
        return octal


    def split_array(self, array):
        octales = []
        for i in range(0, len(array), 3):
            octales.append(array[i:i + 3])
        return octales


    def letra_a_numeros(self, letra):
        dict_hex = {"A": "10", "B": "11", "C": "12", "D": "13", "E": "14", "F": "15"}
        if letra in dict_hex:
            return int(dict_hex[letra])
        else:
            return int(letra)


    def to_binario(self, numero):
        resultado = []
        if numero == 0:
            return [0]
        while numero > 1:
            resultado.append(numero % 2)
            numero = numero // 2
        resultado.append(numero)
        return self.invertir(resultado)


    def invertir(self, arreglo):
        invertido = []
        for i in range(len(arreglo) - 1, -1, -1):
            invertido.append(arreglo[i])
        return invertido


    #print(hex_to_octal("FFFF"))